<%@ page import="java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="au.com.itelasoft.cinema.models.Movie" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="favicon.ico">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Cinema | Home</title>
</head>

<body>
    <!-- Import Header -->
    <jsp:include page="header.jsp"/>

    <div class="container">
        <div class="row mt-4">
            <jsp:useBean id="MOVIE_LIST" scope="request" type="java.util.List"/>
            <c:forEach var="movie" items="${MOVIE_LIST}">
                <div class="col-6 mb-3 test">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="m-0">${movie.movieName}</h5>
                        </div>
                        <div class="card-body">
                            <p class="movie-card-description">${movie.movieDescription}</p>

                            <!-- Create a unique link with movie id -->
                            <c:url var="movieDetailsLink" value="MovieControllerServlet">
                                <c:param name="command" value="DETAILS"/>
                                <c:param name="movieId" value="${movie.movieId}" />
                            </c:url>

                            <a href="${movieDetailsLink}" class="btn btn-info float-right">More Info</a>
                        </div>
                    </div>
                </div>
            </c:forEach>
        </div>
    </div>

    <!-- Import Bootstrap, Popper and Jquery -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>