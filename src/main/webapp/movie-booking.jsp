<%@ page import="java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="favicon.ico">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Cinema | Booking</title>
</head>

<body>
<!-- Import Header -->
<jsp:include page="header.jsp"/>
<% String image = (String) request.getAttribute("IMAGE"); %>

<div class="container text-center">
    <div class="container mt-4">
        <img src="images/<%= image %>" width="150px" alt="movie">
        <h3 class="mt-4">${MOVIE_NAME}</h3>
        <p>22 July 2020 | 10:00 AM</p>
        <hr/>
        <p>Select Your Seat(s)</p>
    </div>
    <form action="BookingControllerServlet" method="post">
    <jsp:useBean id="SEAT_DETAILS_MAP" scope="request" type="java.util.HashMap"/>
    <c:forEach var="seat" items="${SEAT_DETAILS_MAP}">
        <div class="form-check form-check-inline">
            <input class="form-check-input seat-checkbox" type="checkbox" name="seat-${seat.key}" id="seat-${seat.key}" onchange="seatCounter(this.id)"  ${seat.value ? 'disabled checked' : null}>
            <label class="form-check-label" for="seat-${seat.key}">Seat No ${seat.key}</label>
        </div>
    </c:forEach>
        <input type="hidden" name="movieId" value="${MOVIE_ID}">
        <input type="hidden" name="cineplexId" value="${CINEPLEX_ID}">
        <input type="hidden" name="showDate" value="${SHOW_DATE}">
        <input type="hidden" name="startTime" value="${START_TIME}">
        <input type="submit" id="btn-confirm-booking" value="Confirm Booking" class="btn btn-info m-4" disabled>
    </form>
</div>

<script>
    let seatCount = 0;
    function seatCounter(changedId) {
        if(document.getElementById(changedId).checked) {
            seatCount++;
        } else {
            seatCount--;
        }
        if(seatCount > 3) {
            document.getElementById(changedId).checked = false;
            alert("You are not allowed to reserve more than 3 seats");
            seatCount--;
        }
        if(seatCount <= 0) {
            document.getElementById("btn-confirm-booking").disabled = true;
        } else {
            document.getElementById("btn-confirm-booking").disabled = false;
        }
    }
</script>

<!-- Import Bootstrap, Popper and Jquery -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
</body>
</html>