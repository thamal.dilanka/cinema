<jsp:useBean id="MOVIE" scope="request" type="au.com.itelasoft.cinema.models.Movie"/>
<%@ page import="java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="au.com.itelasoft.cinema.models.Movie" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="favicon.ico">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Cinema | Booking</title>
</head>

<body>
<!-- Import Header -->
<jsp:include page="header.jsp"/>
<%Movie movie = (Movie) request.getAttribute("MOVIE");%>

<div class="container">
    <div class="row mt-4">
        <div class="col-md-4">
            <img src="images/<%= movie.getImage() %>" width="350px" alt="movie">
        </div>
        <div class="col-md-8">
            <h4>${MOVIE.movieName}</h4>
            <hr/>
            <p>${MOVIE.movieDescription}</p><br/>
            <h4>Show Times</h4>
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Date</th>
                    <th scope="col">Time</th>
                    <th scope="col">Available Seats</th>
                    <th scope="col">Booking</th>
                </tr>
                </thead>
                <tbody>
                <jsp:useBean id="SHOW_TIMES" scope="request" type="java.util.List"/>
                <c:forEach var="showTime" items="${SHOW_TIMES}">
                    <tr>
                        <td>${showTime.showDate}</td>
                        <td>${showTime.startTime} - ${showTime.endTime}</td>
                        <td>10</td>
                        <td>
                            <!-- Create a unique link with movie id -->
                            <c:url var="movieBooking" value="BookingControllerServlet">
                                <c:param name="movieId" value="${showTime.movieId}" />
                                <c:param name="movie_name" value="${MOVIE.movieName}" />
                                <c:param name="movie_description" value="${MOVIE.movieDescription}" />
                                <c:param name="image" value="${MOVIE.image}" />
                                <c:param name="cineplexId" value="${showTime.cineplexId}" />
                                <c:param name="showDate" value="${showTime.showDate}" />
                                <c:param name="startTime" value="${showTime.startTime}" />
                            </c:url>
                            <a href="${movieBooking}" class="btn btn-info btn-sm">Book Now</a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Import Bootstrap, Popper and Jquery -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
</body>
</html>