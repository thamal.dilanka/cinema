package au.com.itelasoft.cinema.servlets;

import au.com.itelasoft.cinema.dao.MovieDaoImp;
import au.com.itelasoft.cinema.dao.ShowTimeDaoImp;
import au.com.itelasoft.cinema.models.Movie;
import au.com.itelasoft.cinema.models.ShowTime;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/MovieControllerServlet")
public class MovieControllerServlet extends HttpServlet {

    private MovieDaoImp movieDaoImp = new MovieDaoImp();
    private ShowTimeDaoImp showTimeDaoImp = new ShowTimeDaoImp();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String command = request.getParameter("command");

        if(command == null) {
            List<Movie> movies = null;
            try {
                movies = movieDaoImp.getRecords();
            } catch (SQLException exception) {
                exception.printStackTrace();
            }
            request.setAttribute("MOVIE_LIST", movies);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/index.jsp");
            dispatcher.forward(request, response);

        } else if(command.equals("DETAILS")) {
            Movie movie = null;
            ArrayList<ShowTime> showTimes = null;
            int movieId = Integer.parseInt(request.getParameter("movieId"));
            try {
                movie = movieDaoImp.getRecord(movieId);
                showTimes = showTimeDaoImp.getRecords(movieId);
            } catch (SQLException exception) {
                exception.printStackTrace();
            }
            request.setAttribute("MOVIE", movie);
            request.setAttribute("SHOW_TIMES", showTimes);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/movie-details.jsp");
            dispatcher.forward(request, response);
        }
    }
}
