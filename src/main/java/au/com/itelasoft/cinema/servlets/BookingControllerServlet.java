package au.com.itelasoft.cinema.servlets;

import au.com.itelasoft.cinema.dao.BookingDaoImp;
import au.com.itelasoft.cinema.dao.ShowTimeDaoImp;
import au.com.itelasoft.cinema.models.Booking;
import au.com.itelasoft.cinema.models.ShowTime;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.HashMap;

@WebServlet("/BookingControllerServlet")
public class BookingControllerServlet extends HttpServlet {
    ShowTimeDaoImp showTimeDaoImp = new ShowTimeDaoImp();
    BookingDaoImp bookingDaoImp = new BookingDaoImp();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ShowTime showTime = new ShowTime();
        showTime.setMovieId(Integer.parseInt(request.getParameter("movieId")));
        showTime.setCineplexId(Integer.parseInt(request.getParameter("cineplexId")));
        showTime.setShowDate(Date.valueOf(request.getParameter("showDate")));
        showTime.setStartTime(Time.valueOf(request.getParameter("startTime")));

        HashMap<Integer, Boolean> seatDetailsMap = null;

        try {
            seatDetailsMap = showTimeDaoImp.getSeatDetails(showTime);
        } catch (SQLException exception) {
            exception.printStackTrace();
        }

        request.setAttribute("SEAT_DETAILS_MAP", seatDetailsMap);
        request.setAttribute("MOVIE_ID", request.getParameter("movieId"));
        request.setAttribute("MOVIE_NAME", request.getParameter("movie_name"));
        request.setAttribute("MOVIE_DESCRIPTION", request.getParameter("movie_description"));
        request.setAttribute("IMAGE", request.getParameter("image"));
        request.setAttribute("CINEPLEX_ID", request.getParameter("cineplexId"));
        request.setAttribute("SHOW_DATE", request.getParameter("showDate"));
        request.setAttribute("START_TIME", request.getParameter("startTime"));
        RequestDispatcher dispatcher = request.getRequestDispatcher("/movie-booking.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // Get selected seat list
        ArrayList<Integer> selectedSeatNumbers = new ArrayList<>();
        for(int i = 1; i <=10; i++) {
            if(request.getParameter("seat-" + i) != null) {
                selectedSeatNumbers.add(i);
            }
        }

        // Inserting new booking to the DB
        Booking newBooking = new Booking();
        newBooking.setCustomerId(1); // Should be replaced with session details
        newBooking.setCineplexId(Integer.parseInt(request.getParameter("cineplexId")));
        newBooking.setMovieId(Integer.parseInt(request.getParameter("movieId")));
        newBooking.setShowDate(Date.valueOf(request.getParameter("showDate")));
        newBooking.setStartTime(Time.valueOf(request.getParameter("startTime")));

        Booking insertedBooking = null;

        try {
            insertedBooking = bookingDaoImp.insertRecord(newBooking);
        } catch (SQLException exception) {
            exception.printStackTrace();
        }

        // Inserting seats to the DB
        try {
            bookingDaoImp.insertSeats(insertedBooking.getBookingId(), Integer.parseInt(request.getParameter("cineplexId")), selectedSeatNumbers);
        } catch (SQLException exception) {
            exception.printStackTrace();
        }

        RequestDispatcher dispatcher = request.getRequestDispatcher("/booking-confirm.jsp");
        dispatcher.forward(request, response);
    }
}
