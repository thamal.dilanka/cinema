package au.com.itelasoft.cinema.dao;

import au.com.itelasoft.cinema.models.Movie;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public interface MovieDao {
    ArrayList getRecords() throws SQLException;
    Movie getRecord(int id) throws SQLException;
    Movie getLastRecord() throws SQLException;
    Movie insertRecord(Movie movie) throws SQLException;
    Movie updateRecord(int movieId, Movie movie) throws SQLException;
    void deleteRecord(int id) throws SQLException;
    ArrayList getModelList(ResultSet resultSet) throws SQLException;
    Movie getModel(ResultSet resultSet) throws SQLException;
}
