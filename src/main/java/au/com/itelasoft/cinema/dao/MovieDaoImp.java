package au.com.itelasoft.cinema.dao;

import au.com.itelasoft.cinema.models.Movie;
import au.com.itelasoft.cinema.util.DatabaseConnection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class MovieDaoImp implements MovieDao {

    ShowTimeDaoImp showTimeDaoImp = new ShowTimeDaoImp();

    @Override
    public ArrayList<Movie> getRecords() throws SQLException {
        String query = "SELECT * FROM movie";
        Statement statement = DatabaseConnection.getDatabaseConnection().createStatement();
        ResultSet resultSet = statement.executeQuery(query);

        return getModelList(resultSet);
    }

    @Override
    public Movie getRecord(int moveId) throws SQLException {
        String query = "SELECT * FROM movie WHERE movie_id = ?";
        PreparedStatement preparedStatement = DatabaseConnection.getDatabaseConnection().prepareStatement(query);
        preparedStatement.setInt(1, moveId);
        ResultSet resultSet = preparedStatement.executeQuery();

        if(!resultSet.next()) {
            throw new SQLException("Couldn't fetch movie for given id");
        }
        return getModel(resultSet);
    }

    @Override
    public Movie getLastRecord() throws SQLException {
        String query = "SELECT * FROM movie ORDER BY movie_id DESC LIMIT 1";
        PreparedStatement preparedStatement = DatabaseConnection.getDatabaseConnection().prepareStatement(query);
        ResultSet resultSet = preparedStatement.executeQuery();

        if(!resultSet.next()) {
            throw new SQLException("Couldn't fetch movie for given id");
        }
        return getModel(resultSet);
    }

    @Override
    public Movie insertRecord(Movie movie) throws SQLException {
        String query = "INSERT INTO movie(movie_name, description, image) VALUES (?, ?, ?)";
        PreparedStatement preparedStatement = DatabaseConnection.getDatabaseConnection().prepareStatement(query);
        preparedStatement.setString(1, movie.getMovieName());
        preparedStatement.setString(2, movie.getMovieDescription());
        preparedStatement.setString(3, movie.getImage());

        if(preparedStatement.executeUpdate() == 0) {
            throw new SQLException("Insertion failed! Please try again.");
        }
        return getLastRecord();
    }

    @Override
    public Movie updateRecord(int movieId, Movie movie) throws SQLException {

        String query = "UPDATE movie SET movie_name = ?, description = ?, image = ? WHERE movie_id = ?";
        PreparedStatement preparedStatement = DatabaseConnection.getDatabaseConnection().prepareStatement(query);
        preparedStatement.setString(1, movie.getMovieName());
        preparedStatement.setString(2, movie.getMovieDescription());
        preparedStatement.setString(3, movie.getImage());
        preparedStatement.setInt(4, movieId);

        if(preparedStatement.executeUpdate() == 0) {
            throw new SQLException("Update failed! Please try again");
        }
        return getRecord(movieId);
    }

    @Override
    public void deleteRecord(int movieId) throws SQLException {

        String query = "DELETE FROM movie WHERE movie_id = ?";
        PreparedStatement preparedStatement = DatabaseConnection.getDatabaseConnection().prepareStatement(query);
        preparedStatement.setInt(1, movieId);

        if(preparedStatement.executeUpdate() == 0) {
            throw new SQLException("Deletion failed");
        }
    }

    @Override
    public ArrayList<Movie> getModelList(ResultSet resultSet) throws SQLException{
        ArrayList<Movie> movies = new ArrayList<>();
        while (resultSet.next()) {
            Movie movie = getModel(resultSet);
            movie.setShowTimes(showTimeDaoImp.getRecords(resultSet.getInt("movie_id")));
            movies.add(movie);
        }
        return movies;
    }

    @Override
    public Movie getModel(ResultSet resultSet) throws SQLException {
        Movie movie = new Movie();

        movie.setMovieId(resultSet.getInt("movie_id"));
        movie.setMovieName(resultSet.getString("movie_name"));
        movie.setMovieDescription(resultSet.getString("description"));
        movie.setImage(resultSet.getString("image"));
        movie.setShowTimes(showTimeDaoImp.getRecords(resultSet.getInt("movie_id")));
        return movie;
    }
}
