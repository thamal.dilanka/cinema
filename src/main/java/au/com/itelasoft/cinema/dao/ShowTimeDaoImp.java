package au.com.itelasoft.cinema.dao;

import au.com.itelasoft.cinema.models.ShowTime;
import au.com.itelasoft.cinema.util.DatabaseConnection;

import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

public class ShowTimeDaoImp implements ShowTimeDao {

    @Override
    public ArrayList<ShowTime> getRecords() throws SQLException {
        String query = "SELECT * FROM show_time";
        Statement statement = DatabaseConnection.getDatabaseConnection().createStatement();
        ResultSet resultSet = statement.executeQuery(query);

        return getModelList(resultSet);
    }

    @Override
    public ArrayList<ShowTime> getRecords(int movieId) throws SQLException {
        String query = "SELECT * FROM show_time WHERE movie_id = ?";
        PreparedStatement preparedStatement = DatabaseConnection.getDatabaseConnection().prepareStatement(query);
        preparedStatement.setInt(1, movieId);
        ResultSet resultSet = preparedStatement.executeQuery();

        return getModelList(resultSet);
    }

    @Override
    public ShowTime getLastRecord() throws SQLException {
        String query = "SELECT * FROM show_time ORDER BY movie_id DESC LIMIT 1";
        PreparedStatement preparedStatement = DatabaseConnection.getDatabaseConnection().prepareStatement(query);
        ResultSet resultSet = preparedStatement.executeQuery();

        if(!resultSet.next()) {
            throw new SQLException("Couldn't fetch movie for given id");
        }
        return getModel(resultSet);
    }

    @Override
    public ShowTime insertRecord(ShowTime showTime) throws SQLException {
        String query = "INSERT INTO show_time(movie_id, cineplex_id, show_date, start_time, end_time) VALUES (?, ?, ?, ?, ?)";
        PreparedStatement preparedStatement = DatabaseConnection.getDatabaseConnection().prepareStatement(query);
        preparedStatement.setInt(1, showTime.getMovieId());
        preparedStatement.setInt(2, showTime.getCineplexId());
        preparedStatement.setDate(3, showTime.getShowDate());
        preparedStatement.setTime(4, showTime.getStartTime());
        preparedStatement.setTime(5, showTime.getEndTime());

        if(preparedStatement.executeUpdate() == 0) {
            throw new SQLException("Insertion failed! Please try again.");
        }
        return getLastRecord();
    }

    @Override
    public ShowTime updateRecord(int id) throws SQLException {
        return null;
    }

    @Override
    public ShowTime deleteRecord(int id) throws SQLException {
        return null;
    }

    @Override
    public ArrayList<ShowTime> getModelList(ResultSet resultSet) throws SQLException {
        ArrayList<ShowTime> showTimes = new ArrayList<>();
        while (resultSet.next()) {
            ShowTime showTime = getModel(resultSet);
            showTimes.add(showTime);
        }
        return showTimes;
    }

    @Override
    public ShowTime getModel(ResultSet resultSet) throws SQLException {
        ShowTime showTime = new ShowTime();

        showTime.setMovieId(resultSet.getInt("movie_id"));
        showTime.setCineplexId(resultSet.getInt("cineplex_id"));
        showTime.setShowDate(resultSet.getDate("show_date"));
        showTime.setStartTime(resultSet.getTime("start_time"));
        showTime.setEndTime(resultSet.getTime("end_time"));

        return showTime;
    }

    public ArrayList<Integer> getReservedSeats(ShowTime showTime) throws SQLException {
        String query = "SELECT bs.seat_id AS seat_id FROM booking bk INNER JOIN booking_seat bs ON bk.booking_id = bs.booking_id " +
                "WHERE bk.movie_id = ? AND bk.cineplex_id = ? AND bk.show_date = ? AND bk.start_time = ?";
        PreparedStatement preparedStatement = DatabaseConnection.getDatabaseConnection().prepareStatement(query);
        preparedStatement.setInt(1, showTime.getMovieId());
        preparedStatement.setInt(2, showTime.getCineplexId());
        preparedStatement.setDate(3, showTime.getShowDate());
        preparedStatement.setTime(4, showTime.getStartTime());

        ResultSet resultSet = preparedStatement.executeQuery();

        ArrayList<Integer> seatIds = new ArrayList<>();
        while (resultSet.next()) {
            seatIds.add(resultSet.getInt("seat_id"));
        }
        return seatIds;
    }

    public HashMap<Integer, Boolean> getSeatDetails(ShowTime showTime) throws SQLException {
        String query = "SELECT seat_id FROM seat WHERE cineplex_id = ?";
        PreparedStatement preparedStatement = DatabaseConnection.getDatabaseConnection().prepareStatement(query);
        preparedStatement.setInt(1, showTime.getCineplexId());

        ResultSet resultSet = preparedStatement.executeQuery();
        ArrayList<Integer> reservedSeats = getReservedSeats(showTime);

        HashMap<Integer, Boolean> seatDetails = new HashMap<>();
        while (resultSet.next()) {
            int seatId = resultSet.getInt("seat_id");
            seatDetails.put(seatId, reservedSeats.contains(seatId));
        }

        return seatDetails;
    }
}
