package au.com.itelasoft.cinema.dao;

import au.com.itelasoft.cinema.models.ShowTime;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public interface ShowTimeDao {
    ArrayList getRecords() throws SQLException;
    ArrayList getRecords(int id) throws SQLException;
    ShowTime getLastRecord() throws SQLException;
    ShowTime insertRecord(ShowTime showTime) throws SQLException;
    ShowTime updateRecord(int id) throws SQLException;
    ShowTime deleteRecord(int id) throws SQLException;
    ArrayList getModelList(ResultSet resultSet) throws SQLException;
    ShowTime getModel(ResultSet resultSet) throws SQLException;
}