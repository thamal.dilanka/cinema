package au.com.itelasoft.cinema.dao;

import au.com.itelasoft.cinema.models.Booking;
import au.com.itelasoft.cinema.util.DatabaseConnection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class BookingDaoImp implements BookingDao {

    public Booking insertRecord(Booking booking) throws SQLException {
        String query = "INSERT INTO booking(customer_id, movie_id, cineplex_id, show_date, start_time) VALUES (?, ?, ?, ?, ?)";
        PreparedStatement preparedStatement = DatabaseConnection.getDatabaseConnection().prepareStatement(query);
        preparedStatement.setInt(1, booking.getCustomerId());
        preparedStatement.setInt(2, booking.getMovieId());
        preparedStatement.setInt(3, booking.getCineplexId());
        preparedStatement.setDate(4, booking.getShowDate());
        preparedStatement.setTime(5, booking.getStartTime());

        if (preparedStatement.executeUpdate() == 0) {
            throw new SQLException("Insertion failed! Please try again.");
        }
        return getLastRecord();
    }

    public Booking getLastRecord() throws SQLException {
        String query = "SELECT * FROM booking ORDER BY movie_id DESC LIMIT 1";
        PreparedStatement preparedStatement = DatabaseConnection.getDatabaseConnection().prepareStatement(query);
        ResultSet resultSet = preparedStatement.executeQuery();

        if (!resultSet.next()) {
            throw new SQLException("Couldn't fetch movie for given id");
        }
        return getModel(resultSet);
    }

    public Booking getModel(ResultSet resultSet) throws SQLException {
        Booking booking = new Booking();

        booking.setBookingId(resultSet.getInt("booking_id"));
        booking.setCustomerId(resultSet.getInt("customer_id"));
        booking.setMovieId(resultSet.getInt("movie_id"));
        booking.setCineplexId(resultSet.getInt("cineplex_id"));
        booking.setShowDate(resultSet.getDate("show_date"));
        booking.setStartTime(resultSet.getTime("start_time"));

        return booking;
    }

    public void insertSeats(int bookingId, int cineplexId, ArrayList<Integer> seatNumbers) throws SQLException {

        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("INSERT INTO booking_seat (booking_id, cineplex_id, seat_id) VALUES");
        for (int seatId : seatNumbers) {
            queryBuilder.append("(" + bookingId + ", " + cineplexId + ", " + seatId + "),");
        }
        queryBuilder.deleteCharAt(queryBuilder.length() - 1); // Remove last comma

        System.out.println(queryBuilder.toString());

        Statement statement = DatabaseConnection.getDatabaseConnection().createStatement();
        if(statement.executeUpdate(queryBuilder.toString()) == 0){
            throw new SQLException("Couldn't fetch movie for given id");
        }
    }
}