package au.com.itelasoft.cinema.models;

import com.google.gson.annotations.Expose;

import java.sql.Date;
import java.sql.Time;

public class ShowTime {
    private Integer movieId;
    private Integer cineplexId;
    private String cineplex;
    private Date showDate;
    private Time startTime;
    private Time endTime;

    public Integer getMovieId() {
        return movieId;
    }

    public void setMovieId(Integer movieId) {
        this.movieId = movieId;
    }

    public Integer getCineplexId() {
        return cineplexId;
    }

    public void setCineplexId(Integer cineplexId) {
        this.cineplexId = cineplexId;
    }

    public String getCineplex() {
        return cineplex;
    }

    public void setCineplex(String cineplex) {
        this.cineplex = cineplex;
    }

    public Date getShowDate() {
        return showDate;
    }

    public void setShowDate(Date showDate) {
        this.showDate = showDate;
    }

    public Time getStartTime() {
        return startTime;
    }

    public void setStartTime(Time startTime) {
        this.startTime = startTime;
    }

    public Time getEndTime() {
        return endTime;
    }

    public void setEndTime(Time endTime) {
        this.endTime = endTime;
    }
}
