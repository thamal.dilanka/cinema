package au.com.itelasoft.cinema.util;

import java.util.ArrayList;

public class Envelope {
    private String status;
    private String message;
    private Integer results;
    private Object data;

    public Envelope(String status, String message, Integer results, Object data) {
        this.status = status;
        this.message = message;
        this.results = results;
        this.data = data;
    }
}
