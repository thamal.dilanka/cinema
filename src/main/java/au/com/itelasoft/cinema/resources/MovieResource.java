package au.com.itelasoft.cinema.resources;

import au.com.itelasoft.cinema.dao.MovieDaoImp;
import au.com.itelasoft.cinema.models.Movie;
import au.com.itelasoft.cinema.util.Envelope;
import com.google.gson.Gson;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.SQLException;
import java.util.ArrayList;

@Path("movies")
public class MovieResource {

    MovieDaoImp movieDaoImp = new MovieDaoImp();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMovies() {
        Envelope envelope;
        try {
            ArrayList<Movie> movies = movieDaoImp.getRecords();
            envelope = new Envelope("success", null, movies.size(), movies);
            return Response.status(200).entity(new Gson().toJson(envelope)).build();
        } catch (SQLException exception) {
            envelope = new Envelope("error", exception.getMessage(), null, null);
            return Response.status(500).entity(new Gson().toJson(envelope)).build();
        }
    }

    @GET
    @Path("/{movieId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMovie(@PathParam("movieId") int movieId) {
        Envelope envelope;
        try {
            Movie movie = movieDaoImp.getRecord(movieId);
            envelope = new Envelope("success", null, 1, movie);
            return Response.status(200).entity(new Gson().toJson(envelope)).build();
        } catch (SQLException exception) {
            envelope = new Envelope("error", exception.getMessage(), null, null);
            return Response.status(500).entity(new Gson().toJson(envelope)).build();
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response insetMovie(Movie movie) {
        Envelope envelope;
        try {
            Movie insertedMovie = movieDaoImp.insertRecord(movie);
            envelope = new Envelope("success", null, 1, insertedMovie);
            return Response.status(201).entity(new Gson().toJson(envelope)).build();
        } catch (SQLException exception) {
            envelope = new Envelope("error", exception.getMessage(), null, null);
            return Response.status(400).entity(envelope).build();
        }
    }

    @PUT
    @Path("/{movieId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateMovie(@PathParam("movieId") int movieId, Movie movie) {
        Envelope envelope;
        try {
            Movie updatedMovie = movieDaoImp.updateRecord(movieId, movie);
            envelope = new Envelope("success", null, 1, updatedMovie);
            return Response.status(200).entity(new Gson().toJson(envelope)).build();
        } catch (SQLException exception) {
            envelope = new Envelope("error", exception.getMessage(), null, null);
            return Response.status(400).entity(new Gson().toJson(envelope)).build();
        }
    }

    @DELETE
    @Path("/{movieId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteMovie(@PathParam("movieId") int movieId) {
        Envelope envelope;
        try {
            movieDaoImp.deleteRecord(movieId);
            envelope = new Envelope("success", "Deleted", null, null);
            return Response.status(200).entity(new Gson().toJson(envelope)).build();
        } catch (SQLException exception) {
            envelope = new Envelope("error", exception.getMessage(), null, null);
            return Response.status(400).entity(new Gson().toJson(envelope)).build();
        }
    }
}
